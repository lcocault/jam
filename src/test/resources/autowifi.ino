/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Setup entry point.
 */
void setup()
{
  // Initialize the serial connection
  Serial.begin(115200);

  // Establish connection
  Serial.println("AT+CWJAP=\"AndroidLolo\",\"xxxxxxxx\"");
  Serial.flush();

  // Wait a few seconds and assume connection is established
  delay(4000);
  Serial.println("AT+CIPSTART=\"UDP\",\"192.168.43.26\",2705");
  Serial.flush();

}

/**
 * Main loop.
 */
void loop()
{
  // Wait before sending the data size
  delay(3000);
  Serial.println("AT+CIPSEND=7");
  Serial.flush();
    
  // Wait before sending the data
  delay(3000);
  Serial.println("COS=500");
  Serial.flush();
}
