/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Setup entry point.
 */
void setup() {
  // Initialize the serial port
  Serial.begin(9600);
}

/**
 * Main loop.
 */
void loop() {
  
  // In this sample, the behaviour consists in computing a cosinus
  // and a sinus, apply a coefficient to them and send them on the
  // serial bus as integer values. The following compute 2*62
  // successive values.
  for (double x=0.0; x<6.2; x=x+0.1)
  {
    double cosinus = cos(x)*1024.0; // Compute the raw cosinus
    double sinus = sin(x)*1024.0;   // Compute the raw sinus
    int cosValue = cosinus;         // Convert cosinus as an integer
    int sinValue = sinus;           // Convert sinus as an integer

    // Send the value
    Serial.print("COS=");
    Serial.println(cosValue);
    Serial.print("SIN=");
    Serial.println(sinValue);

    // Wait before the next computation
    delay(500);
  }
}
