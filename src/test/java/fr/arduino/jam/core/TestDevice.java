/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.arduino.jam.core.Device;
import fr.arduino.jam.core.Parameter;
import fr.arduino.jam.core.ParameterOccurrenceList;

public class TestDevice {

    @Test
    public void testBasic() {

        // Constructor
        Device device = new ArduinoDevice("D");

        // Getters
        assertEquals("D", device.getName());
    }

    @Test
    public void testSubscriptionMechanisms() {

        // Mixture
        Parameter p1 = new Parameter("P1");
        Parameter p2 = new Parameter("P2");
        Parameter p3 = new Parameter("P3");
        ParameterOccurrenceList l1 = new ParameterOccurrenceList();
        ParameterOccurrenceList l2 = new ParameterOccurrenceList();
        ParameterOccurrenceList l3 = new ParameterOccurrenceList();

        // Constructor
        ArduinoDevice device = new ArduinoDevice("D");

        // Monitor parameters
        device.monitor(p1);
        device.monitor(p2);

        // Register listeners
        try {
            // Nominal registering
            device.register(p1, l1);
            device.register(p2, l2);
        } catch (Exception e) {
            fail();
        }
        try {
            device.register(p3, l3);
            fail();
        } catch (Exception e) {
            // Should not be possible for an unknown parameter
        }

        // Receive data
        device.processData("P1=123");
        device.processData("P2=456");
        device.processData("P1=789");

        // Check values
        assertEquals(2, l1.getOccurrences().size());
        assertEquals(1, l2.getOccurrences().size());

        // Unregister a parameter
        device.unregister(p1);
        device.processData("P1=666");
        assertEquals(2, l1.getOccurrences().size());

        // Un-monitor a parameter
        device.unmonitor(p2);
        device.processData("P2=666");
        assertEquals(1, l2.getOccurrences().size());
        try {
            device.register(p2, l2);
            fail();
        } catch (Exception e) {
            // Should not be possible for an unknown parameter
        }

        // Finally clear the device
        device.clear();
    }

}
