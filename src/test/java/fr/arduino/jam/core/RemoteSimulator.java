/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.core;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Simulator of a remote Arduino device.
 */
public class RemoteSimulator {

    /** Period at which a parameter occurrence is provided. */
    private static final long WRITE_PERIOD = 500;

    /**
     * Main program of the remote simulator.
     * @param args
     *            First and only argument is the port number
     */
    public static void main(String args[]) {

        try {

            // Datagram socket
            DatagramSocket sock = new DatagramSocket();

            // Connection parameters
            InetAddress host = InetAddress.getByName("localhost");
            int port = Integer.parseInt(args[0]);

            for (int count = 0; count < 10; count++) {

                // Write data
                for (double i = 0.0; i < 2 * Math.PI; i = i + 0.1) {

                    // Wait for next value
                    Thread.sleep(WRITE_PERIOD);

                    // Prepare data
                    String cos = "COS=" + (int) (Math.cos(i) * 1000);
                    String sin = "SIN=" + (int) (Math.sin(i) * 1000);

                    // Send data
                    DatagramPacket dpCos = new DatagramPacket(cos.getBytes(),
                            cos.length(), host, port);
                    DatagramPacket dpSin = new DatagramPacket(sin.getBytes(),
                            sin.length(), host, port);
                    sock.send(dpCos);
                    sock.send(dpSin);
                }
            }

            // Close the socket
            sock.close();

        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }
}
