/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.junit.Test;

public class TestParameter {

    @Test
    public void testBasic() {

        // Constructor
        Parameter param = new Parameter("P");

        // Getters
        assertEquals("P", param.getName());
        assertTrue(param.getProperties().isEmpty());

        // Property management
        param.addProperty("MyKey", "MyValue");
        assertEquals(1, param.getProperties().size());
        assertEquals("MyValue", param.getProperty("MyKey"));
        param.setProperties(new HashMap<String, Object>());
        assertTrue(param.getProperties().isEmpty());
    }

}
