/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.core;


/**
 * Simulator of an Arduino device.
 */
public class DeviceEmulator extends Device implements Runnable {

    /** Period at which a parameter occurrence is provided. */
    private static final long WRITE_PERIOD = 500;

    /** Thread to provide data. */
    private Thread emulatorThread_;

    /** Should the provision of data active ? */
    private boolean active_;

    /**
     * Constructor.
     */
    public DeviceEmulator() {
        super("Emulator");
        // Collection
        active_ = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void connect(String portName) throws ComException {
        // Active
        active_ = true;
        // Run a thread to provide data
        emulatorThread_ = new Thread(this);
        emulatorThread_.start();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void disconnect() {
        // No more active
        active_ = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {

        try {

            while (active_) {

                // Write data
                for (double i = 0.0; i < 2 * Math.PI && active_; i = i + 0.1) {

                    // Wait for next value
                    Thread.sleep(WRITE_PERIOD);

                    // Process data
                    processData("COS=" + (int) (Math.cos(i) * 1000));
                    processData("SIN=" + (int) (Math.sin(i) * 1000));
                }
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
