/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.core;


/**
 * Default implementation for a remote host, without GUI.
 */
public class DefaultRemoteHost implements ParameterListener {

    /**
     * Main program of the remote simulator.
     * @param args
     *            First and only argument is the port number
     */
    public static void main(String args[]) {

        // Default host
        DefaultRemoteHost host = new DefaultRemoteHost();

        // Remote device
        RemoteDevice device = new RemoteDevice("Remotely tested");

        try {
            // Register and monitor the given parameters
            for (int i = 1; i < args.length; i++) {

                // Create the parameter
                Parameter parameter = new Parameter(args[i]);

                // Monitor it
                device.monitor(parameter);

                // And register the current class as a listener
                device.register(parameter, host);
            }

            // Connect the device
            device.connect(args[0]);

        } catch (CoreException | ComException e) {

            // A fatal error occurred
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void receiveParameterOccurrence(ParameterOccurrence occurrence) {
        System.out.println(occurrence.toString());
    }
}
