/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.core;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.Test;

import fr.arduino.jam.core.Parameter;
import fr.arduino.jam.core.ParameterListener;
import fr.arduino.jam.core.ParameterOccurrence;
import fr.arduino.jam.core.ParameterOccurrenceList;

public class TestParameterOccurrencesList {

    @Test
    public void testBasic() {

        // Mixture
        Parameter param = new Parameter("P");
        Date date = new Date();
        int value = 123456;
        ParameterOccurrence occ1 = new ParameterOccurrence(param, date, value);
        ParameterOccurrence occ2 = new ParameterOccurrence(param, date, value);

        // Constructor
        ParameterListener listener = new ParameterOccurrenceList();

        // Feed the listener
        listener.receiveParameterOccurrence(occ1);
        listener.receiveParameterOccurrence(occ2);

        // Check the list
        List<ParameterOccurrence> list = ((ParameterOccurrenceList) listener)
                .getOccurrences();
        assertEquals(2, list.size());
        assertEquals(occ1, list.get(0));
        assertEquals(occ2, list.get(1));
    }
}
