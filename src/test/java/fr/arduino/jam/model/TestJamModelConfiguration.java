/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

public class TestJamModelConfiguration {

    @Test
    public void testBasic() {

        // Constructor
        JamModelConfiguration cfg = new JamModelConfiguration();

        // Getters
        assertEquals("/dev/ttyUSB0", cfg.getDevicePort());
        assertEquals("", cfg.getCssPath());
        assertEquals("", cfg.getMimicPath());
        assertEquals(0, cfg.getParameters().size());
    }

    @Test
    public void testXmlSerialization() {

        // Mixture
        ParameterModel p1 = new ParameterModel("P1");
        ParameterModel p2 = new ParameterModel("P2");
        List<ParameterModel> parameters = new ArrayList<ParameterModel>();
        parameters.add(p1);
        parameters.add(p2);

        // Constructor
        JamModelConfiguration cfg = new JamModelConfiguration();
        JamModelConfiguration parsed = new JamModelConfiguration();

        // Setters
        cfg.setParameters(parameters);
        cfg.setDevicePort("/dev/ttyUSB1");
        cfg.setCssPath("default.css");
        cfg.setMimicPath("default.svg");

        try {

            // Serialize
            JAXBContext jaxbContext = JAXBContext
                    .newInstance(JamModelConfiguration.class);
            Marshaller jaxbMarhsaller = jaxbContext.createMarshaller();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            jaxbMarhsaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarhsaller.marshal(cfg, bos);

            // Parse
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            ByteArrayInputStream bis = new ByteArrayInputStream(
                    bos.toByteArray());
            parsed = (JamModelConfiguration) jaxbUnmarshaller.unmarshal(bis);

            // Check the parsed configuration
            assertEquals(cfg.getDevicePort(), parsed.getDevicePort());
            assertEquals(cfg.getMimicPath(), parsed.getMimicPath());
            assertEquals(2, parsed.getParameters().size());
            assertEquals("P1", parsed.getParameters().get(0).getName());
            assertEquals("P2", parsed.getParameters().get(1).getName());

        } catch (JAXBException e) {
            fail(e.getMessage());
        }
    }

}
