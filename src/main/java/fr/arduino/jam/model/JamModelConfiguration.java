/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Configuration file for the JAM editor.
 */
@XmlRootElement
public class JamModelConfiguration {

    /** Default device path. */
    private static final String DEFAULT_DEVICE_PORT = "/dev/ttyUSB0";

    /** Device port. */
    private String devicePort_;

    /** Mimic path. */
    private String mimicPath_;

    /** CSS path. */
    private String cssPath_;

    /** List of parameters defined for the JAM model. */
    private List<ParameterModel> parameters_;

    /**
     * Constructor.
     */
    public JamModelConfiguration() {
        devicePort_ = DEFAULT_DEVICE_PORT;
        cssPath_ = "";
        mimicPath_ = "";
        parameters_ = new ArrayList<ParameterModel>();
    }

    /**
     * Get the port of the device.
     * @return Device port
     */
    public String getDevicePort() {
        return devicePort_;
    }

    /**
     * Get the path of the CSS file.
     * @return CSS path
     */
    public String getCssPath() {
        return cssPath_;
    }

    /**
     * Get the path of the mimic file.
     * @return Mimic path
     */
    public String getMimicPath() {
        return mimicPath_;
    }

    /**
     * Get the list of known parameters for the model.
     * @return the list of known parameters
     */
    public List<ParameterModel> getParameters() {
        return parameters_;
    }

    /**
     * Set the port of the device.
     * @param devicePort
     *            Device port
     */
    public void setDevicePort(final String devicePort) {
        devicePort_ = devicePort;
    }

    /**
     * Set the path of the CSS file.
     * @param cssPath
     *            CSS path
     */
    public void setCssPath(final String cssPath) {
        cssPath_ = cssPath;
    }

    /**
     * Set the path of the mimic file.
     * @param mimicPath
     *            Mimic path
     */
    public void setMimicPath(final String mimicPath) {
        mimicPath_ = mimicPath;
    }

    /**
     * Set the list of known parameters for the model.
     * @param parameters
     *            the list of known parameters
     */
    @XmlElement
    public void setParameters(final List<ParameterModel> parameters) {
        parameters_ = parameters;
    }

}
