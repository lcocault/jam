/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.model;

import java.io.File;
import java.util.HashSet;
import java.util.Observable;
import java.util.Set;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import fr.arduino.jam.core.ComException;
import fr.arduino.jam.core.CoreException;
import fr.arduino.jam.core.Device;
import fr.arduino.jam.core.Parameter;
import fr.arduino.jam.core.ParameterListener;

/**
 * Main model for the JAM editor.
 */
public class JamModel extends Observable {

    /** Null template file. */
    private static final File NULL_TEMPLATE_FILE = new File("");

    /** Name of the template file. */
    private ObjectProperty<File> templateFile_;

    /** Device connected. */
    private Device device_;

    /** Configuration of the JAM model. */
    private JamModelConfiguration cfg_;

    /** Set of listeners. */
    private Set<ParameterListener> listeners_;

    /**
     * Constructor.
     * @param deviceConnector
     *            Connector to the device
     * @throws ClassNotFoundException
     *             The device connector cannot be found
     * @throws IllegalAccessException
     *             The connector is not accessible
     * @throws InstantiationException
     *             The device connector cannot be instantiated
     */
    public JamModel(final String deviceConnector)
        throws ClassNotFoundException, InstantiationException,
        IllegalAccessException {
        // Load the connector
        device_ = (Device) ClassLoader.getSystemClassLoader()
                .loadClass(deviceConnector).newInstance();
        // Initialize the attributes
        cfg_ = new JamModelConfiguration();
        templateFile_ = new SimpleObjectProperty<File>(NULL_TEMPLATE_FILE);
        listeners_ = new HashSet<ParameterListener>();
    }

    /**
     * Close the current template.
     */
    public void closeTemplate() {

        // Store the name of the file
        templateFile_.set(NULL_TEMPLATE_FILE);

        // Reset the configuration
        cfg_ = new JamModelConfiguration();

        // Disconnect and clear the device
        device_.disconnect();
        device_.clear();
    }

    /**
     * Get the path of the CSS file.
     * @return CSS file
     */
    public String getCssFile() {
        return cfg_.getCssPath();
    }

    /**
     * Get the path of the mimic file.
     * @return Mimic file
     */
    public String getMimicFile() {
        return cfg_.getMimicPath();
    }

    /**
     * Get the currently edited template file.
     * @return Template file currently being edited (null if no project is being
     *         edited)
     */
    public final File getTemplateFile() {
        return templateFile_.get();
    }

    /**
     * Open a new template and connect the device.
     * @param template
     *            Path of the template to open
     */
    public void openTemplate(final File template) {

        // Store the name of the file
        templateFile_.set(template);

        // Parse the file and initialize the model
        try {

            // Parse the file to build the configuration
            final JAXBContext context = JAXBContext
                    .newInstance(JamModelConfiguration.class);
            final Unmarshaller unmarshaller = context.createUnmarshaller();
            cfg_ = (JamModelConfiguration) unmarshaller.unmarshal(template);

            // Connect the device
            device_.connect(cfg_.getDevicePort());

            // Monitors the parameters
            for (ParameterModel parameter : cfg_.getParameters()) {
                device_.monitor(parameter);
            }

            // And register the listeners
            for (ParameterListener listener : listeners_) {
                registerOnDevice(listener);
            }

        } catch (ComException | JAXBException e) {
            e.printStackTrace();
        }

        // Notify observers
        setChanged();
        notifyObservers();
    }

    /**
     * Register the listener to all known parameters.
     * @param listener
     *            Listener to register
     */
    public void register(final ParameterListener listener) {
        // Keep the list of listeners
        listeners_.add(listener);
        // Register on the device
        registerOnDevice(listener);
    }

    /**
     * Register the listener to all known parameters on the device.
     * @param listener
     *            Listener to register
     */
    private void registerOnDevice(final ParameterListener listener) {
        try {
            // Register the listener for each parameter
            for (Parameter parameter : cfg_.getParameters()) {
                device_.register(parameter, listener);
            }
        } catch (CoreException e) {
            e.printStackTrace();
        }
    }

    /**
     * Save the current template.
     */
    public void saveTemplate() {
        // TODO Auto-generated method stub
    }

    /**
     * Get the template file property.
     * @return Template file property (null if no project is being edited)
     */
    public final ObjectProperty<File> templateFileProperty() {
        return templateFile_;
    }

}
