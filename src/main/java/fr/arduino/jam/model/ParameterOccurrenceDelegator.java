/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.model;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import javafx.application.Platform;
import fr.arduino.jam.core.ParameterListener;
import fr.arduino.jam.core.ParameterOccurrence;

/**
 * Delegator of a parameter notification to avoid threading issues between the
 * GUI event thread and the underlying COM thread.
 */
public class ParameterOccurrenceDelegator implements ParameterListener,
        Runnable {

    /** Interface to delegate the processing of received parameter. */
    private ParameterListener delegator_;

    /** Queue of occurrences to process. */
    private Queue<ParameterOccurrence> queue_;

    /**
     * Constructor.
     * @param delegator
     *            Interface to delegate the parameter occurrence processing
     */
    public ParameterOccurrenceDelegator(final ParameterListener delegator) {
        queue_ = new ArrayBlockingQueue<ParameterOccurrence>(16);
        delegator_ = delegator;
    }

    /**
     * Store the parameter occurrence and defer the effective processing.
     * @param occurrence
     *            Parameter occurrence received
     */
    @Override
    public void receiveParameterOccurrence(final ParameterOccurrence occurrence) {
        queue_.add(occurrence);
        Platform.runLater(this);
    }

    /**
     * Delegate the processing of the parameter.
     */
    @Override
    public void run() {
        delegator_.receiveParameterOccurrence(queue_.remove());
    }

}
