/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.core;

import java.util.HashMap;
import java.util.Map;

/**
 * Parameter exchanged with the Arduino device.
 */
public class Parameter {

    /** Name of the parameter. */
    private String name_;

    /** Additional properties. */
    private Map<String, Object> properties_;

    /**
     * Default constructor (for serialization purpose only).
     */
    @Deprecated
    public Parameter() {
        name_ = "Undefined";
        properties_ = new HashMap<String, Object>();
    }

    /**
     * Constructor.
     * @param name
     *            Name of the parameter
     */
    public Parameter(final String name) {
        name_ = name;
        properties_ = new HashMap<String, Object>();
    }

    /**
     * Add a property to the parameter.
     * @param key
     *            Key of the property
     * @param value
     *            Value of the property
     */
    public void addProperty(final String key, final Object value) {
        properties_.put(key, value);
    }

    /**
     * Get the name of the parameter.
     * @return Parameter's name
     */
    public String getName() {
        return name_;
    }

    /**
     * Get the list of additional properties.
     * @return Additional properties
     */

    public Map<String, Object> getProperties() {
        return properties_;
    }

    /**
     * Get a property of the parameter.
     * @param key
     *            Key of the property
     * @return Value of the property
     */
    public Object getProperty(final String key) {
        return properties_.get(key);
    }

    /**
     * Set the name of the parameter.
     * @param name
     *            Parameter's name
     */
    @Deprecated
    public void setName(final String name) {
        name_ = name;
    }

    /**
     * Set the list of additional properties.
     * @param properties
     *            Additional properties
     */
    public void setProperties(final Map<String, Object> properties) {
        properties_ = properties;
    }

}
