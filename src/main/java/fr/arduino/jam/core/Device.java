/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generic device class.
 */
public abstract class Device {

    /** Separator between parameter reference and value. */
    private static final char DATA_SEPARATOR = '=';

    /** Name of the device. */
    private String name_;

    /** Declared parameters. */
    private Map<String, Parameter> parameters_;

    /** Listeners for parameters. */
    private Map<String, List<ParameterListener>> listeners_;

    /**
     * Constructor.
     * @param name
     *            Name of the device
     */
    protected Device(final String name) {
        // Mandatory values
        name_ = name;
        // Collections
        parameters_ = new HashMap<String, Parameter>();
        listeners_ = new HashMap<String, List<ParameterListener>>();
    }

    /**
     * Clear the device.
     */
    public void clear() {
        // Clear the collections
        parameters_.clear();
        listeners_.clear();
    }

    /**
     * Connect the device model to the physical equipment using the given serial
     * port.
     * @param portName
     *            Name of the serial port to connect the device
     * @throws ComException
     *             Cannot connect the device to the physical equipment
     */
    public abstract void connect(final String portName) throws ComException;

    /**
     * Disconnect the device from the physical equipment.
     */
    public abstract void disconnect();

    /**
     * Get the name of the device.
     * @return Device's name
     */
    public String getName() {
        return name_;
    }

    /**
     * Monitor a parameter.
     * @param parameter
     *            Parameter to monitor
     */
    public void monitor(final Parameter parameter) {
        parameters_.put(parameter.getName(), parameter);
    }

    /**
     * Process a received data.
     * @param data
     *            Data received
     */
    protected void processData(final String data) {

        // Get the current date
        final Date now = new Date();

        // Extract the parameter reference and the value
        final int separatorIndex = data.indexOf(DATA_SEPARATOR);
        final String ref = data.substring(0, separatorIndex);
        final String valueStr = data.substring(separatorIndex + 1);
        final int value = Integer.parseInt(valueStr);

        // Get the parameter by its reference
        final Parameter parameter = parameters_.get(ref);

        // The parameter may not be monitored
        if (parameter != null) {

            // Create the parameter occurrence
            final ParameterOccurrence occurrence = new ParameterOccurrence(
                    parameter, now, value);

            // And notify the listeners (if any)
            final List<ParameterListener> listeners = listeners_.get(ref);
            if (listeners != null) {
                for (ParameterListener listener : listeners) {
                    listener.receiveParameterOccurrence(occurrence);
                }
            }
        }
    }

    /**
     * Subscribe a listener to a given parameter.
     * @param parameter
     *            Reference of the parameter to monitor
     * @param listener
     *            Reference of the listener to send received values
     * @throws CoreException
     *             The parameter is unknown
     */
    public void register(final Parameter parameter,
            final ParameterListener listener) throws CoreException {

        // Collections are indexed with the name of the parameter
        final String key = parameter.getName();

        if (parameters_.containsKey(parameter.getName())) {

            // The parameter is supported
            List<ParameterListener> listeners = listeners_.get(key);
            if (listeners == null) {

                // No listener has subscribed to the parameter, create the
                // listeners collection
                listeners = new ArrayList<ParameterListener>();
                listeners_.put(key, listeners);
            }
            listeners.add(listener);

        } else {

            // Unknown parameter
            throw new CoreException("Unknown parameter " + parameter);

        }
    }

    /**
     * Stop monitoring a parameter.
     * @param parameter
     *            Parameter to stop monitoring
     */
    public void unmonitor(final Parameter parameter) {
        parameters_.remove(parameter.getName());
        listeners_.remove(parameter.getName());
    }

    /**
     * Unregister listener for a given parameter. Nothing is done if the
     * parameter was not monitored.
     * @param parameter
     *            Reference of the parameter
     */
    public void unregister(final Parameter parameter) {
        listeners_.remove(parameter.getName());
    }

}
