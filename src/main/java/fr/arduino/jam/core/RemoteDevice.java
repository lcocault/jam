/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.core;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Remotely accessible device.
 */
public class RemoteDevice extends Device implements Runnable {

    /** Buffer size to receive incoming messages. */
    private static final int BUFFER_SIZE = 512;

    /** Server socket. */
    private DatagramSocket server_;

    /**
     * Constructor.
     */
    public RemoteDevice() {
        this("Remote");
    }

    /**
     * Constructor.
     * @param name
     *            Name of the remote device
     */
    public RemoteDevice(final String name) {
        super(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void connect(final String portName) throws ComException {

        try {

            // Listen incoming connections on the given port
            final int port = Integer.parseInt(portName);
            server_ = new DatagramSocket(port);

            // Run the "accept" in a separate thread
            new Thread(this).start();

        } catch (IOException e) {

            // Cannot open the port
            throw new ComException("Cannot open port", e);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void disconnect() {

        if (server_ != null) {
            // Close the server socket
            server_.close();
            server_ = null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {

        try {
            // Create a datagram packet to receive incoming data
            final byte[] buffer = new byte[BUFFER_SIZE];
            final DatagramPacket pck = new DatagramPacket(buffer, BUFFER_SIZE);

            // While the server has not been disconnected, read messages
            while (server_ != null) {

                // Receive a packet
                server_.receive(pck);

                // Extract data
                final byte[] data = pck.getData();
                final String message = new String(data, 0, pck.getLength());

                // Process the data
                processData(message);
            }

        } catch (IOException e) {
            // End of connection
        }
    }
}
