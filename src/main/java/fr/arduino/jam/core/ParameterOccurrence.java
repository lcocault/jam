/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.core;

import java.util.Date;

/**
 * Occurrence of a parameter exchanged with the Arduino device.
 */
public class ParameterOccurrence {

    /** Reference to the parameter. */
    private Parameter parameter_;

    /** Date of the parameter. */
    private Date date_;

    /** Value of the parameter. */
    private int value_;

    /**
     * Constructor.
     * @param parameter
     *            Reference to the parameter
     * @param date
     *            Date of the parameter
     * @param value
     *            Value of the parameter
     */
    public ParameterOccurrence(final Parameter parameter, final Date date,
            final int value) {
        parameter_ = parameter;
        date_ = date;
        value_ = value;
    }

    /**
     * Get the reference of the parameter.
     * @return Parameter's reference
     */
    public Parameter getParameter() {
        return parameter_;
    }

    /**
     * Get the date of the occurrence.
     * @return Occurrence's date
     */
    public Date getDate() {
        return date_;
    }

    /**
     * Get the name of the parameter.
     * @return Occurrence's name
     */
    public String getName() {
        return parameter_.getName();
    }

    /**
     * Get the value of the occurrence.
     * @return Occurrence's value
     */
    public int getValue() {
        return value_;
    }

    /**
     * Display the parameter occurrence.
     * @return Serialization of the parameter occurrence
     */
    public String toString() {
        return date_ + " / " + getName() + "=" + value_;
    }

}
