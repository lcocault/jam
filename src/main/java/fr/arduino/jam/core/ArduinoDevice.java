/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.core;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.TooManyListenersException;

/**
 * Arduino based device.
 */
public class ArduinoDevice extends Device implements SerialPortEventListener {

    /** Milliseconds to block while waiting for port open. */
    private static final int TIME_OUT = 2000;

    /** Default bits per second for COM port. */
    private static final int DATA_RATE = 9600;

    /** Serial port used to communicate with the device. */
    private SerialPort port_;

    /** Input stream from the connected device. */
    private BufferedReader input_;

    /** Output stream to the connected device. */
    private OutputStream output_;

    /**
     * Constructor.
     */
    public ArduinoDevice() {
        this("Arduino");
    }

    /**
     * Constructor.
     * @param name
     *            Name of the device
     */
    protected ArduinoDevice(final String name) {
        super(name);
        // Default values
        port_ = null;
        input_ = null;
        output_ = null;
    }

    /**
     * {@inheritDoc}
     */
    public void connect(final String portName) throws ComException {

        // Port identifier
        CommPortIdentifier portId = null;

        // Get the system port identifiers
        final Enumeration<?> portEnum = CommPortIdentifier.getPortIdentifiers();

        // First, Find the instance of the serial port.
        while (portId == null && portEnum.hasMoreElements()) {

            // Current port identifier
            final CommPortIdentifier currPortId = (CommPortIdentifier) portEnum
                    .nextElement();

            // Compare with the expected port
            if (currPortId.getName().equals(portName)) {
                portId = currPortId;
            }
        }

        // If the port is not found, throw an exception
        if (portId == null) {
            throw new ComException("Could not find COM port " + portName);
        }

        try {

            // Open serial port
            port_ = (SerialPort) portId.open(getName(), TIME_OUT);

            // Configure port parameters
            port_.setSerialPortParams(DATA_RATE, SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

            // Open the streams
            input_ = new BufferedReader(new InputStreamReader(
                    port_.getInputStream()));
            output_ = port_.getOutputStream();

            // Add event listeners
            port_.addEventListener(this);
            port_.notifyOnDataAvailable(true);

        } catch (PortInUseException | UnsupportedCommOperationException
                | IOException | TooManyListenersException exception) {

            // Throw a model based exception
            throw new ComException("Cannot connect port " + portName, exception);

        }
    }

    /**
     * {@inheritDoc}
     */
    public synchronized void disconnect() {

        // The disconnection is only relevant if the connection has been
        // established
        if (port_ != null) {

            // Remove the listener and close the connection
            port_.removeEventListener();
            port_.close();

            // Reset the attributes
            port_ = null;
            input_ = null;
            output_ = null;
        }
    }

    /**
     * Handle an event on the serial port. Read the data and print it.
     * @param event
     *            Event received from the serial bus
     */
    @Override
    public synchronized void serialEvent(final SerialPortEvent event) {

        // Check if new data has arrived
        // TODO should consider the other eventTypes.
        if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {

            try {

                // Read the line
                final String inputLine = input_.readLine();

                System.out.println("Received : " + inputLine);

                processData(inputLine);

            } catch (IOException | NumberFormatException exception) {

                // TODO Change the way the error is handled
                System.err.println(exception.toString());
            }
        }
    }

    /**
     * Write a command to the device.
     * @param cmd
     *            Command to write
     * @throws ComException
     *             Cannot write the command to the device
     */
    protected void write(final String cmd) throws ComException {
        try {
            // Write the raw content of the string
            output_.write(cmd.getBytes());

        } catch (IOException e) {

            // Impossible to write the command
            throw new ComException("Cannot write " + cmd + " to " + getName());
        }
    }
}
