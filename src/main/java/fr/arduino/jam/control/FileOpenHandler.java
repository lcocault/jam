/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.control;

import java.util.ResourceBundle;

import fr.arduino.jam.model.JamModel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.FileChooser;

/**
 * Handler called when the Open button of the File menu is clicked.
 */
public class FileOpenHandler implements EventHandler<ActionEvent> {

    /** Main model of the editor. */
    private JamModel model_;

    /** I18n resources. */
    private ResourceBundle i18n_;

    /**
     * Constructor of the handler.
     * @param i18n
     *            I18n resources
     * @param model
     *            State of the Gantt editor
     */
    public FileOpenHandler(final ResourceBundle i18n, final JamModel model) {
        i18n_ = i18n;
        model_ = model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handle(final ActionEvent event) {
        // Create the file chooser
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(i18n_
                .getString("fr.arduino.jam.view.fileOpenTitle"));
        model_.openTemplate(fileChooser.showOpenDialog(null));
    }

}
