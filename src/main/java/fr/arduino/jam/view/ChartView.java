/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.view;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.collections.ObservableList;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import fr.arduino.jam.core.ParameterListener;
import fr.arduino.jam.core.ParameterOccurrence;
import fr.arduino.jam.model.ParameterModel;

/**
 * Chart view for series of parameter occurrences.
 */
public class ChartView implements ParameterListener {

    /** Time step in milliseconds (time scale accuracy). */
    private static final long TIME_STEP = 500L;

    /** Time window in steps. */
    private static final int TIME_WINDOW = 120;

    /** Chart view. */
    private LineChart<Number, Number> chart_;

    /** X axis. */
    private NumberAxis xAxis_;

    /** Y axis. */
    private NumberAxis yAxis_;

    /** List of parameters values. */
    private Map<String, XYChart.Series<Number, Number>> series_;

    /**
     * Constructor.
     * @param i18n
     *            Internationalization resources bundle
     * @param occurrences
     *            List of parameter occurrences to display
     */
    public ChartView(final ResourceBundle i18n,
            final ObservableList<ParameterOccurrence> occurrences) {

        // Now
        final long now = new Date().getTime() / TIME_STEP;

        // Create the chart and the axis
        xAxis_ = new NumberAxis(now, now + TIME_WINDOW, 10);
        yAxis_ = new NumberAxis(0, 10, 2);
        chart_ = new LineChart<Number, Number>(xAxis_, yAxis_);

        // Setup chart
        chart_.setId("parametersChart");
        chart_.setCreateSymbols(false);
        chart_.setAnimated(false);
        chart_.setTitle(i18n.getString("fr.arduino.jam.view.chartTitle"));

        // Congigure axis
        xAxis_.setLabel(i18n.getString("fr.arduino.jam.view.chartX"));
        xAxis_.setForceZeroInRange(false);
        yAxis_.setLabel(i18n.getString("fr.arduino.jam.view.chartY"));
        yAxis_.setForceZeroInRange(true);
        yAxis_.setAutoRanging(true);

        // Create data series
        series_ = new HashMap<String, XYChart.Series<Number, Number>>();
    }

    /**
     * Get the chart view.
     * @return View on parameter chart
     */
    public LineChart<Number, Number> getView() {
        return chart_;
    }

    /**
     * Receive a new parameter occurrence. Draw it on the chart.
     * @param occurrence
     *            Occurrence to draw on the chart
     */
    @Override
    public void receiveParameterOccurrence(final ParameterOccurrence occurrence) {

        // Name, date and value of the parameter
        final ParameterModel param = (ParameterModel) occurrence.getParameter();
        final String name = param.getName();
        final long date = occurrence.getDate().getTime();
        final int value = occurrence.getValue();

        // All the data received are drawn
        XYChart.Series<Number, Number> series = series_.get(name);

        if (series == null) {

            // The series does not exist, just create it
            series = new XYChart.Series<Number, Number>();
            series.setName(name);

            // Add the series to the chart
            series_.put(name, series);
            chart_.getData().add(series);
        }

        // Adjust the time window
        if (series.getData().size() >= TIME_WINDOW) {
            xAxis_.setLowerBound(xAxis_.getLowerBound() + 1);
            xAxis_.setUpperBound(xAxis_.getUpperBound() + 1);
        }

        // Add data
        series.getData().add(
                new XYChart.Data<Number, Number>(date / TIME_STEP, value));
    }

}
