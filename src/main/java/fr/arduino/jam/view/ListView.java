/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.view;

import java.util.ResourceBundle;

import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import fr.arduino.jam.core.ParameterOccurrence;

/**
 * View on the list of parameter occurrences.
 */
public class ListView {

    /** Parameter occurrences view. */
    private TableView<ParameterOccurrence> occurrencesView_;

    /**
     * Constructor.
     * @param i18n
     *            Internationalization resources bundle
     * @param occurrences
     *            List of parameter occurrences to display
     */
    public ListView(final ResourceBundle i18n,
            final ObservableList<ParameterOccurrence> occurrences) {

        // Create the view and associate the model
        occurrencesView_ = new TableView<ParameterOccurrence>();
        occurrencesView_.setItems(occurrences);

        // Name column
        final TableColumn<ParameterOccurrence, String> nameCol = new TableColumn<ParameterOccurrence, String>(
                i18n.getString("fr.arduino.jam.view.parameterName"));
        nameCol.setCellValueFactory(new PropertyValueFactory<ParameterOccurrence, String>(
                "name"));

        // Date column
        final TableColumn<ParameterOccurrence, String> startCol = new TableColumn<ParameterOccurrence, String>(
                i18n.getString("fr.arduino.jam.view.parameterDate"));
        startCol.setCellValueFactory(new PropertyValueFactory<ParameterOccurrence, String>(
                "date"));

        // Value column
        final TableColumn<ParameterOccurrence, String> endCol = new TableColumn<ParameterOccurrence, String>(
                i18n.getString("fr.arduino.jam.view.parameterValue"));
        endCol.setCellValueFactory(new PropertyValueFactory<ParameterOccurrence, String>(
                "value"));

        // Add the columns
        occurrencesView_.getColumns().add(nameCol);
        occurrencesView_.getColumns().add(startCol);
        occurrencesView_.getColumns().add(endCol);
    }

    /**
     * Get the occurrences view.
     * @return View on parameter occurrences list
     */
    public TableView<ParameterOccurrence> getView() {
        return occurrencesView_;
    }

}
