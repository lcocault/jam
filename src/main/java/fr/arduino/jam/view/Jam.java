/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.view;

import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import fr.arduino.jam.core.ParameterOccurrence;
import fr.arduino.jam.model.JamModel;
import fr.arduino.jam.model.ParameterOccurrenceDelegator;
import fr.arduino.jam.model.ParameterOccurrenceRelay;

/**
 * Main application of the JAM tool editor.
 */
public class Jam extends Application implements Observer {

    /** Default device connector. */
    private static final String DEFAULT_CONNECTOR = "fr.arduino.jam.core.ArduinoDevice";

    /** Default height of the window in pixels. */
    private static final int DEFAULT_HEIGHT = 800;

    /** Default width of the window in pixels. */
    private static final int DEFAULT_WIDTH = 1200;

    /** Keyword of the property that specifies the device connector. */
    private static final String CONNECTOR_KEYWORD = "connector";

    /** Keyword of the property that specifies the window height. */
    private static final String HEIGHT_KEYWORD = "height";

    /** Keyword of the property that specifies the window width. */
    private static final String WIDTH_KEYWORD = "width";

    /** Name of the JAM style-sheet. */
    private static final String JAM_STYLESHEET = "jam.css";

    /** I18n resources. */
    private ResourceBundle i18n_;

    /** Main model. */
    private JamModel model_;

    /** Scene of the application. */
    private Scene scene_;

    /**
     * Run the main application.
     * @param args
     *            Arguments of the application
     */
    public static void main(final String[] args) {
        // Launch the GUI
        launch(args);
    }

    /**
     * Create the scene for the application.
     * @return Created scene
     */
    private Scene createScene() {

        // Default size of the window
        int width = DEFAULT_WIDTH;
        int height = DEFAULT_HEIGHT;

        // Get size from the properties
        if (System.getProperty(WIDTH_KEYWORD) != null) {
            width = Integer.parseInt(System.getProperty(WIDTH_KEYWORD));
        }
        if (System.getProperty(HEIGHT_KEYWORD) != null) {
            height = Integer.parseInt(System.getProperty(HEIGHT_KEYWORD));
        }

        // Scene it
        return new Scene(new VBox(), width, height);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(final Stage primaryStage) throws Exception {

        // Initialize the resources
        i18n_ = ResourceBundle.getBundle("I18n");

        // Create the model
        String connector = DEFAULT_CONNECTOR;
        if (System.getProperty(CONNECTOR_KEYWORD) != null) {
            connector = System.getProperty(CONNECTOR_KEYWORD);
        }
        model_ = new JamModel(connector);
        model_.addObserver(this);

        // Set the main window title
        primaryStage.titleProperty().bind(
                Bindings.concat(i18n_.getString("fr.arduino.jam.view.title"),
                        model_.templateFileProperty()));

        // Scene it
        scene_ = createScene();
        scene_.getStylesheets().add(JAM_STYLESHEET);

        // Widgets of the view
        final MainMenuBar menuBar = new MainMenuBar(i18n_, model_);
        final SplitPane pane = new SplitPane();
        final SplitPane txtPane = new SplitPane();
        final SplitPane gfxPane = new SplitPane();
        pane.setDividerPosition(0, 0.3);
        txtPane.setOrientation(Orientation.VERTICAL);
        gfxPane.setOrientation(Orientation.VERTICAL);

        // Occurrences list view
        final ObservableList<ParameterOccurrence> occurrences = FXCollections
                .observableArrayList();
        final ListView occurrencesListView = new ListView(i18n_, occurrences);
        model_.register(new ParameterOccurrenceRelay(occurrences));
        txtPane.getItems().add(occurrencesListView.getView());

        // Mimic view
        final MimicView mimicView = new MimicView();
        final ParameterOccurrenceDelegator mimicDelegator = new ParameterOccurrenceDelegator(
                mimicView);
        model_.addObserver(mimicView);
        model_.register(mimicDelegator);
        gfxPane.getItems().add(mimicView.getView());

        // Chart view
        final ChartView chartView = new ChartView(i18n_, occurrences);
        final ParameterOccurrenceDelegator chartDelegator = new ParameterOccurrenceDelegator(
                chartView);
        model_.register(chartDelegator);
        gfxPane.getItems().add(chartView.getView());

        // Add the widgets
        pane.getItems().add(txtPane);
        pane.getItems().add(gfxPane);
        ((VBox) scene_.getRoot()).getChildren().add(menuBar);
        ((VBox) scene_.getRoot()).getChildren().add(pane);

        // Show the editor
        primaryStage.setScene(scene_);
        primaryStage.show();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop() throws Exception {

        // Release the resources allocated by the model
        model_.closeTemplate();

        // Stop the application
        super.stop();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(final Observable model, final Object event) {

        // The model has been updated, refresh the style sheet (by replacing the
        // existing styles)
        scene_.getStylesheets().clear();
        scene_.getStylesheets().add(JAM_STYLESHEET);
        if (model_.getCssFile() != null && !model_.getCssFile().isEmpty()) {
            scene_.getStylesheets().add(model_.getCssFile());
        }
    }
}
