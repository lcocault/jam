/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.view;

import java.util.ResourceBundle;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import fr.arduino.jam.control.FileCloseHandler;
import fr.arduino.jam.control.FileOpenHandler;
import fr.arduino.jam.control.FileSaveHandler;
import fr.arduino.jam.model.JamModel;

/**
 * Menu bar for the Jam editor.
 */
public class MainMenuBar extends MenuBar {

    /**
     * Constructor.
     * @param i18n
     *            Internationalization resources
     * @param model
     *            Main model of the editor
     */
    public MainMenuBar(final ResourceBundle i18n, final JamModel model) {

        // Create the menu file
        final Menu menuFile = new Menu(
                i18n.getString("fr.arduino.jam.view.menuFile"));

        // File open
        final MenuItem menuFileOpen = new MenuItem(
                i18n.getString("fr.arduino.jam.view.menuFileOpen"));
        menuFileOpen.setOnAction(new FileOpenHandler(i18n, model));
        menuFile.getItems().add(menuFileOpen);

        // File close
        final MenuItem menuFileClose = new MenuItem(
                i18n.getString("fr.arduino.jam.view.menuFileClose"));
        menuFileClose.setOnAction(new FileCloseHandler(model));
        menuFileClose.disableProperty().bind(
                model.templateFileProperty().isNull());
        menuFile.getItems().add(menuFileClose);

        // File save
        final MenuItem menuFileSave = new MenuItem(
                i18n.getString("fr.arduino.jam.view.menuFileSave"));
        menuFileSave.setOnAction(new FileSaveHandler(model));
        menuFileSave.disableProperty().bind(
                model.templateFileProperty().isNull());
        menuFile.getItems().add(menuFileSave);

        getMenus().add(menuFile);
    }
}
