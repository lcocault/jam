/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.arduino.jam.view;

import java.io.File;
import java.text.MessageFormat;
import java.util.Observable;
import java.util.Observer;

import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import fr.arduino.jam.core.ParameterListener;
import fr.arduino.jam.core.ParameterOccurrence;
import fr.arduino.jam.model.JamModel;

/**
 * View of the mimics. This view is based on an SVG display.
 */
public class MimicView implements Observer, ParameterListener {

    /** Pattern of the main JavaScript function. */
    private static final MessageFormat JS_FCT = new MessageFormat(
            "setParameterValue(\"{0}\", {1,number,####})");

    /** Embedded view rendering the mimic. */
    private WebView view_;

    /**
     * Constructor.
     */
    public MimicView() {

        // Load a default and empty
        view_ = new WebView();

        // Load the empty page
        final File empty = new File("default.svg");
        view_.getEngine().load("file://" + empty.getAbsolutePath());
    }

    /**
     * Get the web view rendering the mimic.
     * @return Embedded web view
     */
    public WebView getView() {
        return view_;
    }

    /**
     * Receive a new parameter occurrence. Call the JavaScript main function.
     * @param occurrence
     *            Occurrence to draw on the chart
     */
    @Override
    public void receiveParameterOccurrence(final ParameterOccurrence occurrence) {

        // Web engine
        final WebEngine engine = view_.getEngine();

        // Apply the JavaScript calling pattern
        final Object[] args = new Object[] {occurrence.getName(),
                occurrence.getValue()};
        final String fct = JS_FCT.format(args);

        // Execute the function
        engine.executeScript(fct);
    }

    /**
     * Update the view depending on the model. {@inheritDoc}
     */
    @Override
    public void update(final Observable observable, final Object event) {

        if (observable instanceof JamModel) {

            // Get the model
            final JamModel model = (JamModel) observable;

            // Reload the mimic
            final WebEngine engine = view_.getEngine();
            final File empty = new File(model.getMimicFile());
            engine.load("file://" + empty.getAbsolutePath());

            // Activate the JavaScript engine
            engine.setJavaScriptEnabled(true);
        }
    }
}
