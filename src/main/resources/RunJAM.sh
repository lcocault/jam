# Copyright 2015 Laurent COCAULT
# Licensed to Laurent COCAULT under one or more contributor license agreements.
# See the NOTICE file distributed with this work for additional information
# regarding copyright ownership. Laurent COCAULT licenses this file to You
# under the Apache License, Version 2.0 (the "License"); you may not use this
# file except in compliance with the License.  You may obtain a copy of the
# License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#!/bin/bash

# Resource paths
export RXTX_LIB=/home/manu/Applications/rxtx-2.1-7-bins-r2/Linux/i686-unknown-linux-gnu
export JAVA_HOME=/home/manu/Applications/jdk1.8.0_51

# Dimensions of the JAM window. The dimensions of the screen are used, but the
# height is adjusted in case there is a desktop toolbar
export HEIGHT=`xrandr | grep '*' | cut -f2 -dx | cut -f1 -d' '`
export WIDTH=`xrandr | grep '*' | cut -f1 -dx | tr -d ' '`
export HEIGHT=$(($HEIGHT-50))

# Run the JAM application
sudo $JAVA_HOME/bin/java -cp .:$RXTX_LIB/rxtx-2.1.7.jar -Djava.library.path=$RXTX_LIB -Dwidth=$WIDTH -Dheight=$HEIGHT fr.arduino.jam.view.Jam
