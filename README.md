# Scope

The JAM editor is a basic monitor for Arduino based devices.

#License

It is licensed by Laurent COCAULT under the Apache License Version 2.0. A copy
of this license is provided in the LICENSE.txt file.

# Content

The src/main/java directory contains the library sources.
The src/main/resources directory contains the library data.
The src/test/java directory contains the tests sources.
The src/test/resources directory contains the tests data.

# Dependencies

JAM editor relies on the following free software, all released under business
friendly free licenses.

permanent dependency:

  - RXTX 2.1.7 from Alexander Graf and others
    https://github.com/rxtx/
    released under the Lesser General Public License Version 2.1

test-time dependency:

  - JUnit 4.12 from Erich Gamma and Kent Beck
    http://www.junit.org/
    released under the Common Public License Version 1.0
